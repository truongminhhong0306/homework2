
# coding: utf-8

# In[12]:


for x in range(101):
    if x == 0:
        continue
    if x % 3 == 0 and x % 5 != 0:
        print('fizz')
    elif x % 3 != 0 and x % 5 == 0:
        print('buzz')
    elif x % 15 == 0:
        print('fizzbuzz')
    else:
        print(x)

